﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrefeituraDigitalAPI.Models
{
    public class AdocaoRepository : IAdocaoRepository
    {
        private List<Adocao> adocaos = new List<Adocao>();
        private int _nextId = 1;

        public AdocaoRepository()
        {
            Add(new Adocao { Cpf = "123.456.790-12", Endereco = "Rua São Roque, 1480", Email = "fferreira3d@gmail.com", Cao = true });
        }

        public IEnumerable<Adocao> GetAll()
        {
            return adocaos;
        }

        public Adocao Get(int id)
        {
            return adocaos.Find(p => p.Id == id);
        }

        public Adocao Add(Adocao item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            item.Data = System.DateTime.Now;
            adocaos.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            adocaos.RemoveAll(p => p.Id == id);
        }

        public bool Update(Adocao item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = adocaos.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            adocaos.RemoveAt(index);
            adocaos.Add(item);
            return true;
        }
    }
}