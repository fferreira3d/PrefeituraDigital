﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrefeituraDigitalAPI.Models
{
    interface ISolicitacaoRepository
    {
        IEnumerable<Solicitacao> GetAll();
        Solicitacao Get(int id);
        Solicitacao Add(Solicitacao item);
        void Remove(int id);
        bool Update(Solicitacao item);
    }
}
