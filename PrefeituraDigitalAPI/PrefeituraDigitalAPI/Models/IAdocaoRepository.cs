﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrefeituraDigitalAPI.Models
{
    interface IAdocaoRepository
    {
        IEnumerable<Adocao> GetAll();
        Adocao Get(int id);
        Adocao Add(Adocao item);
        void Remove(int id);
        bool Update(Adocao item);
    }
}
