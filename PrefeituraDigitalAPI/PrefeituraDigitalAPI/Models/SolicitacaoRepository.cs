﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrefeituraDigitalAPI.Models
{
    public class SolicitacaoRepository : ISolicitacaoRepository
    {
        private List<Solicitacao> solicitacaos = new List<Solicitacao>();
        private int _nextId = 1;

        public SolicitacaoRepository()
        {
            Add(new Solicitacao { Tipo = Solicitacao.TIPOS[0], Logradouro = "Rua São Roque" });
            Add(new Solicitacao { Tipo = Solicitacao.TIPOS[2], Logradouro = "Rua Gustavo da Silveira" });
            Add(new Solicitacao { Tipo = Solicitacao.TIPOS[3], Logradouro = "Rua Santo Amaro" });
            Add(new Solicitacao { Tipo = Solicitacao.TIPOS[4], Logradouro = "Rua Conselheiro Lafaiete" });
        }

        public IEnumerable<Solicitacao> GetAll()
        {
            return solicitacaos;
        }

        public Solicitacao Get(int id)
        {
            return solicitacaos.Find(p => p.Id == id);
        }

        public Solicitacao Add(Solicitacao item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            item.Data = System.DateTime.Now;
            solicitacaos.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            solicitacaos.RemoveAll(p => p.Id == id);
        }

        public bool Update(Solicitacao item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = solicitacaos.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            solicitacaos.RemoveAt(index);
            solicitacaos.Add(item);
            return true;
        }
    }
}