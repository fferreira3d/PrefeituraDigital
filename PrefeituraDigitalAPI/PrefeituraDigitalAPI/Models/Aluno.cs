﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrefeituraDigitalAPI.Models
{
    public class Aluno
    {
        public int Matricula { get; set; }
        public string Nome { get; set; }
        public string[] Historico { get; set; }
    }
}