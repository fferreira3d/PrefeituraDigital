﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrefeituraDigitalAPI.Models
{
    public class Adocao
    {
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Endereco { get; set; }
        public string Email { get; set; }
        public Boolean Cao { get; set; }
        public Boolean Gato { get; set; }
        public DateTime Data { get; set; }
    }
}