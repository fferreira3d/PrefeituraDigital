﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrefeituraDigitalAPI.Models
{
    public class Solicitacao
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Logradouro { get; set; }
        public DateTime Data { get; set; }

        public static String[] TIPOS = new string[] {   "1-Poda de Árvore" ,
                                                        "2-Construção de Meio-Fio",
                                                        "3-Recolhimento de Carros Abandonados",
                                                        "4-Desobstrução de Vias Públicas",
                                                        "5-Desobstrução de Córregos",
                                                        "6-Coleta de Animal Morto",
                                                        "7-Limpeza de Boca de Lobo" };
    }
}