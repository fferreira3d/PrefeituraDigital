﻿using PrefeituraDigitalAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrefeituraDigitalAPI.Controller
{
    public class AdocaoController : ApiController
    {
        static readonly IAdocaoRepository repository = new AdocaoRepository();
        public AdocaoController()
        {

        }

        public IEnumerable<Adocao> GetAll()
        {
            return repository.GetAll();
        }

        public IHttpActionResult GetById(int id)
        {
            var adocao = repository.Get(id);
            if (adocao == null)
            {
                return NotFound();
            }
            return Ok(adocao);
        }

        public IHttpActionResult PutAdocao(String cpf, String endereco, String email, Boolean cao, Boolean gato)
        {
            Adocao adocao = new Adocao { Cpf = cpf, Endereco = endereco, Email = email, Cao = cao, Gato = gato };
            repository.Add(adocao);
            return Ok(adocao);
        }
    }
}
