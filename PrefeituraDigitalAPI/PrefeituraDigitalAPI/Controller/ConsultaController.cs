﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrefeituraDigitalAPI.Controller
{
    public class ConsultaController : ApiController
    {
        public ConsultaController () {

        }

        [HttpGet]
        [Route("api/Consulta/iptu/{id}")]
        public IHttpActionResult GetIptu(long id)
        {
            Random random = new Random();
            return Ok((random.NextDouble() * (1000 - id)) + id);
        }
        [HttpGet]
        [Route("api/Consulta/coletaLixo/{logradouro}")]
        public IHttpActionResult GetDiasDeColetaDeLixo(string logradouro)
        {
            if(logradouro.LastIndexOf('a') % 2 == 0)
                return Ok("Seg,Qua,Sex");
            else
                return Ok("Ter,Qui,Sab");
        }
    }
}
