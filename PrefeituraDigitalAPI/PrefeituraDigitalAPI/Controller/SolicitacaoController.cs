﻿using PrefeituraDigitalAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrefeituraDigitalAPI.Controller
{
    public class SolicitacaoController : ApiController
    {
        static readonly ISolicitacaoRepository repository = new SolicitacaoRepository();
        public SolicitacaoController() {
            
        }

        public IEnumerable<Solicitacao> GetAll()
        {
            return repository.GetAll();
        }

        public IHttpActionResult GetById(int id)
        {
            var soliciatacao = repository.Get(id);
            if (soliciatacao == null)
            {
                return NotFound();
            }
            return Ok(soliciatacao);
        }

        public IHttpActionResult Put(String tipo, String logradouro)
        {
            var soliciatacao = new Solicitacao { Tipo = tipo, Logradouro = logradouro };
            repository.Add(soliciatacao);
            return Ok(soliciatacao);
        }

        [HttpGet]
        [Route("api/Solicitacao/GetTipos")]
        public IHttpActionResult GetTipos()
        {
            return Ok(Solicitacao.TIPOS);
        }
    }
}
