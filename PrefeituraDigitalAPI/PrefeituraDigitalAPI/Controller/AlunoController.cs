﻿using PrefeituraDigitalAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrefeituraDigitalAPI.Controllers
{
    public class AlunoController : ApiController
    {
        Aluno[] alunos = new Aluno[]
        {
            new Aluno { Matricula = 1, Nome = "Jorge", Historico = new string[] { "Pre-Escola A", "Colegio A", "Faculdade C" } },
            new Aluno { Matricula = 2, Nome = "Pedro", Historico = new string[] { "Pre-Escola C", "Colegio A" } },
            new Aluno { Matricula = 3, Nome = "David", Historico = new string[] { "Pre-Escola B", "Colegio B", "Faculdade A", "Faculdade B" } }
        };

        public IEnumerable<Aluno> GetAllAlunos()
        {
            return alunos;
        }

        [HttpGet]
        [Route("api/Aluno/{matricula}/Historico")]
        public IHttpActionResult GetHistoricoAluno(int matricula)
        {
            var aluno = alunos.FirstOrDefault((p) => p.Matricula == matricula);
            if (aluno == null)
            {
                return NotFound();
            }
            return Ok(aluno.Historico);
        }
    }
}
