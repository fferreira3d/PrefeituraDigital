# Laboratório PUC - Oferta 5 - PPDS - Aula 03
###### Curso de Arquitetura de Software Distribuído - Oferta 5
###### Disciplina de Produtividade no Desenvolvimento de Software

----
# Respostas ao roteiro de atividades


### Questão 1 - O que é o estilo arquitetural de API Web e como ele pode operar nas organizações?
Com o aumento dos dispositivos moveis fez se necessário a evolução dos portais WEB de forma a atender todas as requisições de forma mais ampla. As aplicações são divididas em APIs de serviços que são consumidas via REST (abstração do protocolo HTTP). Ou seja, o padrão arquitetural monolítico é subdivido em APIs de serviços que são acessadas de maneira diferente por dispositivos moveis e desktop, através destas requisições REST.

### Questão 3 - Que outras tecnologias de mercado poderiam ser usadas para criar arquiteturas de API?
1. SpringBoot
1. Node.Js
1. NancyFX

### Questão 5 - Métodos HTTP usados para cada uma das operações abaixo no [projeto exemplo](http://www.asp.net/web-api/overview/getting-started-with-aspnet-web-api/tutorial-your-first-web-api), que códigos HTTP o servidor Web retornou para cada operação?
###### Listagem de produtos:
 - A URI http://localhost:50718/api/products utilizada com o método GET retorna a lista de produtos (com todas informações) no padrão JSON.

###### Listagem de um produto específico:
 - A URI http://localhost:50718/api/products/2 utilizada com o método GET retorna as informações de um único produto no padrão JSON.

###### Inserção de um novo produto:
 - A URI http://localhost:50718/api/products utilizada com o método PUT retorna uma mensagem de erro: "The requested resource does not support http method 'PUT'"

###### Alteração dos dados de um produto:
 - A URI http://localhost:50718/api/products/2 utilizada com o método POST retorna uma mensagem de erro: "The requested resource does not support http method 'POST'"

###### Remoção de um produto:
 - A URI http://localhost:50718/api/products/2 utilizada com o método DELETE retorna uma mensagem de erro: "The requested resource does not support http method 'DELETE'"

### Questão 6 - Qual a relação entre o estilo arquitetural de APIs Web com o estilo de micro-serviços? Justifique.
O estilo arquitetural de microserviços é geralmente consumido por requisições REST, cada serviço ou conjunto de serviços pode ser visto como uma API WEB.

### Questão 7 - O livro [API for Dummies](https://pages.apigee.com/ebook-apis-for-dummies-reg.html) recomenda uma estrutura de times para operacionalizar o estilo arquitetural de APIs? Como é essa estrutura e qual o racional da sua utilidade com as estruturas convencionais de desenvolvimento que vemos nas empresas de TI do Brasil?


### Questão 8 - Especifique uma API de serviços para um cenário de uma transformação digital de um prefeitura, composto por serviços de utilidade pública para cidadãos.
A API foi desenvolvida em um unico projeto do Visual Studio 2015, e foi baseada no guia de [melhores praticas](https://github.com/Microsoft/api-guidelines/blob/master/Guidelines.md) para desenvolvimento de de REST APIs da Microsoft.
Este projeto ainda não possui nenhuma tela, foi utilizado da ferramenta PostMan para validação e testes das requisições.
Abaixo a lista de requisições que podem ser consumidas:

| **Request Service CLIENTE** | **Method** | **Params** | **Return** | **Description** |
|:---|:---:|:---:|:---:|:---|
| http://localhost:50311/api/consulta/iptu/{id} | ***GET*** | id | Imposto a ser pago | Consulta do valor do IPTU a ser pago |
| http://localhost:50311/api/consulta/coletaLixo/{logradouro} | ***GET*** | logradouro | Horarios e dias de coleta | Consulta de horário da Coleta de Lixo |
| http://localhost:50311/api/solicitacao/GetTipos | ***GET*** | | | Lista dos tipos de solicitações que podem ser feitas |
| http://localhost:50311/api/solicitacao | ***PUT*** | tipo / logradouro |  | Cria uma nova solicitação |
| http://localhost:50311/api/solicitacao/{id} | ***GET*** | id | Solicitação | Consulta o andamento da solicitação |
| http://localhost:50311/api/solicitacao | ***GET*** | | | Consulta a lista de todas as solicitações feitas |
| http://localhost:50311/api/aluno/{matricula}/historico | ***GET*** | matricula | Histórico Escolar | Consulta o histórico escolar do aluno |
| http://localhost:50311/api/aluno | ***GET*** | | Lista de Alunos | Consulta lista de alunos matriculados |
| http://localhost:50311/api/adocao | ***PUT*** | cpf / endereco / email / cao / gato | |  Pedido de adoção de cães e gatos |
| http://localhost:50311/api/adocao | ***GET*** |  | |  Lista de pedidos de adoção |